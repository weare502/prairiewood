<?php
/**
 * Template part for displaying Dynamic Pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

get_header(); ?>
<!-- Start taxonomy-pwdynamic.php -->
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php
		$term = $wp_query->get_queried_object();


		$testimonials = new WP_Query( array(
				
				//Type & Status Parameters
				'post_type'   => 'pwtestimonial',
				//Order & Orderby Parameters
				'orderby'             => 'rand',
				//Pagination Parameters
				'posts_per_page'         => 1,
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			)
		);

		$blog_posts = new WP_Query( array(
				
				//Type & Status Parameters
				'post_type'   => 'post',
				//Order & Orderby Parameters
				'orderby'             => 'rand',
				//Pagination Parameters
				'posts_per_page'         => 1,
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			)
		);

		$properties = new WP_Query( array(
				
				//Type & Status Parameters
				'post_type'   => 'pwproperty',
				//Order & Orderby Parameters
				'orderby'             => 'rand',
				//Pagination Parameters
				'posts_per_page'         => -1,
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			)
		);

		$dynamic_loop = array_merge($properties->posts, $testimonials->posts, $blog_posts->posts);

		$blocks = new WP_Query( array(
				
				//Type & Status Parameters
				'post_type'   => 'pwdynamicblock',
				//Order & Orderby Parameters
				'orderby'             => 'rand',
				//Pagination Parameters
				'posts_per_page'         => -1,
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			)
		);

		$dynamic_loop = array_merge( $dynamic_loop, $blocks->posts );

		$packages = new WP_Query( array( 
				'post_type' => 'pwpackage',
				'posts_per_page' => -1,
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			) 
		);

		// old stuff
		// $dynamic_loop = array_merge( $dynamic_loop, $packages->posts );

		// calculate number of blocks we already have so we can calc # of photos
		$dynamic_loop = array_merge( $blocks->posts, $packages->posts, $testimonials->posts );

		$pictures = new WP_Query( array(
				
				//Type & Status Parameters
				'post_type'   => 'attachment',
				// Attachments don't have a status
				'post_status' => 'any',
				//Order & Orderby Parameters
				'orderby'             => 'rand',
				//Pagination Parameters
				'posts_per_page'         => ( 16 - count($dynamic_loop) ),
				//Taxonomy Parameters
				'tax_query' => array(
					array(
						'taxonomy'         => 'pwdynamic',
						'terms'            => array( $term->term_id ),
						'operator'         => 'IN'
					),
				),
			)
		);

		$dynamic_loop = array_merge($dynamic_loop, $pictures->posts);

		// Randomize order of all items...JK
		// $dynamic_loop = sort_array_random_separate( $dynamic_loop, 'post_type' );

		$dynamic_loop = array_merge( $blocks->posts, $packages->posts, $pictures->posts, $testimonials->posts );

	?>

	<article <?php post_class(); ?>>
		<?php $term_image = wp_get_attachment_image_src( get_term_meta( $term->term_id, 'image', true ), 'full' ); ?>
		<header class="entry-header" style="background-image: url(<?php echo $term_image[0]; ?>);">
			<div class="title-wrap">
				<h1 class="entry-title"><?php echo $term->name; ?></h1>
				<div class="sub-title">at Prairiewood</div>
			</div>
		</header><!-- .entry-header -->

		<div class="entry-content">

		<div class="dynamic-wrapper">
			<?php foreach ( $dynamic_loop as $post ) : ?>
				<?php $template = $post->post_type; 
						$altbg = get_post_meta( $post->ID, 'pwproperty_dynamic_image_' . $term->term_id, true );
						$bg_arr = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
						$bg = '';
						if ( ! empty( $altbg ) ){
							$bg = $altbg;
						} else {
							$bg = $bg_arr[0];
						}
				?>
				<div class="inline template-<?php echo $post->post_type; ?>" data-template="<?php echo $template . '-' . $post->ID; ?>" style="background-image:url(<?php echo $bg; ?>);">
					<div class="inner">
					<?php switch ($template) {
						case 'pwproperty': ?>
							<div class="title"><?php echo get_the_title($post->ID); ?></div>
							<div class="tagline">at Prairiewood</div>
							<?php break;
						case 'pwtestimonial': ?>
							<div class="quote"><?php echo prairiewood_limit_words( $post->post_content, rand(20, 25) ); ?></div>
							<div class="info">
								<span class="name"><?php echo get_the_title( $post->ID ); ?></span>
								<?php $location = get_post_meta( $post->ID, 'pwtestimonial_location', true ); ?>
								<?php if ( $location ) : ?>
									<span> - </span>
									<span class="location"><?php echo esc_html( $location ); ?></span>
								<?php endif; ?>
							</div>
							<?php break;
						case 'attachment' :
							break;
						case 'post' : ?>
							<div class="from">From the Blog</div>
							<div class="title"><?php echo get_the_title( $post->ID ); ?></div>
							<?php break;
						default: ?>
							<div class="title"><?php echo get_the_title( $post->ID ); ?></div>
							<?php break;
					} ?>
					</div>
				</div>
			<?php endforeach; wp_reset_postdata(); ?>
		</div><!-- end wrapper -->         
		
		<?php foreach ( $dynamic_loop as $post ): ?>
			<template type="text/template" id="template-<?php echo $post->post_type; ?>-<?php echo $post->ID ?>">
				<div class="expanded-block template-<?php echo $post->post_type; ?>" >
					<div class="inner">
						<?php switch ($post->post_type) {
							case 'pwproperty': ?>
								<?php 
										$big_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
										$altbg = get_post_meta( $post->ID, 'pwproperty_dynamic_image_' . $term->term_id, true );
										
										if ( ! empty( $altbg ) ) :
									?>
										
										<img src="<?php echo $altbg; ?>" />

									<?php else : ?>
									
										<img src="<?php echo $big_image[0]; ?>" width="<?php echo $big_image[1]; ?>" height="<?php echo $big_image[2]; ?>" />

									<?php endif; ?>
								<div>
									<h1 class="post-title"><?php echo $post->post_title; ?></h1>
									<div><?php echo wpautop( get_post_meta( $post->ID, 'pwproperty_dynamic_' . $term->term_id, true ) ); ?></div>
									<a href="<?php echo get_the_permalink( $post->ID ); ?>" class="button"><?php echo get_option( $post->post_type . '_details_button_text', 'View More Details' ); ?></a>
								</div>
								<?php break;
							case 'pwpackage': ?>
								<?php $big_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
								<img src="<?php echo $big_image[0]; ?>" width="<?php echo $big_image[1]; ?>" height="<?php echo $big_image[2]; ?>" />
								<div>
									<h1 class="post-title"><?php echo $post->post_title; ?></h1>
									<div><?php echo wpautop( get_post_meta( $post->ID, 'pwpackage_dynamic_' . $term->term_id, true ) ); ?></div>
									<a href="<?php echo get_the_permalink( $post->ID ); ?>" class="button"><?php echo get_option( $post->post_type . '_details_button_text', 'View More Details' ); ?></a>
								</div>
								<?php break;
							case 'pwtestimonial': ?>
								<blockquote>
									<div class="quote"><?php echo wpautop( $post->post_content ); ?></div>
									<div class="info">
										<span class="name"><?php echo get_the_title( $post->ID ); ?></span>
										<?php $location = get_post_meta( $post->ID, 'pwtestimonial_location', true ); ?>
										<?php if ( $location ) : ?>
											<span> - </span>
											<span class="location"><?php echo esc_html( $location ); ?></span>
										<?php endif; ?>
										<br>
										<?php $stayed_at = get_post_meta( $post->ID , '_custom_post_type_onomies_relationship', true ); ?>
										<strong class="where"><?php echo get_the_title( $stayed_at ); ?> Guest</strong>
									</div>
								</blockquote>
								<?php break;
							case 'attachment': ?>
								<?php $big_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
								<img src="<?php echo $big_image[0]; ?>" class="attachment-image" width="<?php echo $big_image[1]; ?>" height="<?php echo $big_image[2]; ?>" />
								<?php break;
							case 'pwdynamicblock': ?>
								<?php $big_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); 
									$block_video = get_field( 'pwdynamicblock_video' );
								?>
								<?php if ( $block_video ) : ?>
									<div class="video-wrapper">
										<?php echo $block_video; ?>
									</div>
								<?php elseif ( $big_image ) : ?>
									<img src="<?php echo $big_image[0]; ?>" width="<?php echo $big_image[1]; ?>" height="<?php echo $big_image[2]; ?>" />
								<?php endif; ?>
								<div>
									<h1 class="post-title"><?php echo $post->post_title; ?></h1>
									<div><?php echo wpautop( $post->post_content ); ?></div>
									<?php 
										$block_link_id = get_post_meta( $post->ID, 'pwdynamicblock_link', true );
										if ( $block_link_id ) :
											$block_link = get_the_permalink( $block_link_id ); ?>
												<a href="<?php echo $block_link; ?>" class="button"><?php echo get_option( $post->post_type . '_details_button_text', 'View More Details' ); ?></a>
										<?php endif; ?>
								</div>
								<?php break;
							case 'post': ?>
								<?php $big_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
								<?php if ( $big_image ) : ?>
									<img src="<?php echo $big_image[0]; ?>" width="<?php echo $big_image[1]; ?>" height="<?php echo $big_image[2]; ?>" />
								<?php endif; ?>
								<div>
									<h1 class="post-title"><?php echo $post->post_title; ?></h1>
									<?php echo get_excerpt_by_id( $post ); ?>
								</div>
								<?php break;
							default:
								# code
								break;
						} ?>
					</div>
				</div>
			</template>
		<?php endforeach; wp_reset_postdata(); ?>

		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'prairiewood' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->

	</main><!-- #main -->
</div><!-- #primary -->
<!-- End taxonomy-pwdynamic.php -->
<?php get_footer(); ?>