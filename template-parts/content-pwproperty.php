<?php
/**
 * Template part for displaying properties.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-bottom: 0;">
	<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
		<div class="title-wrap">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<div class="sub-title">at Prairiewood</div>
		</div>
	</header><!-- .entry-header -->

	

	<div class="entry-content content-wrapper">

		<div id="property-main-content">

			<div class="property-gallery clear">
				<?php $gallery = get_post_meta( get_the_ID(), 'pwproperty_image_gallery', true ); 
					$gallery_ids = array();
					
					if ( $gallery ) :

						foreach ( $gallery as $key => $value ){
							$gallery_ids[] = $key;
						}
						$gallery_ids = implode(',', $gallery_ids);

						soliloquy_dynamic( array( 
							'id' => 'custom-property-images-' . get_the_ID(), 
							'images' => $gallery_ids, 
							'thumbnails' => true,
							'slider_height' => 600,
							'slider' => 1,
						) ); 

					endif;	?>
					<?php $gallery_link = get_post_meta( get_the_ID(), 'pwproperty_photo_gallery_link', true ); ?>
					<?php if ( $gallery_link ) : ?>
						<?php $gallery_link = get_term_link( (int) $gallery_link, 'media_category' ); ?>
						<div class="centered-text"><p><a href="<?php echo $gallery_link; ?>">View Complete Photo Gallery</a></p></div>
					<?php endif; ?>
					
			</div>

				<?php the_content(); ?>

				<hr class="sep" />

				<?php $key_features = get_post_meta( get_the_ID(), 'pwproperty_key_features', true ); ?>

				<?php if ( $key_features ) : ?>
					<div class="key-features sidebar-item">
						<h3 class="sidebar-title">Key Features</h3>
						<ul>
						<?php foreach ( $key_features as $key ) : ?>
							<li class="key-feature"><span class="inner"><?php echo $key; ?></span></li>
						<?php endforeach; ?>
						</ul>
					</div>

				<?php endif; ?>

				<?php $amenities = get_post_meta( get_the_ID(), 'pwproperty_amenities', true );

					if ( $amenities ) : ?>
						<div class="amenities sidebar-item">
							<h3 class="sidebar-title">Amenities</h3>
							<ul>
								<?php foreach ( $amenities as $amenity ) : ?>
									<li class="amenity"><span class="inner"><?php echo $amenity; ?></span></li>
								<?php endforeach; ?>
							</ul>
						</div>
				<?php endif; ?>

					<?php 
						// so we can reference it from the loop
						$post_id = get_the_ID();
						
						$args = array(
							//Type & Status Parameters
							'post_type'   => 'pwtestimonial',
							'post_status' => 'publish',
							
							//Pagination Parameters
							'posts_per_page'         => 1,
							'orderby' => 'rand',
							
							//Custom Field Parameters
							'meta_key'       => '_custom_post_type_onomies_relationship',
							'meta_value'     => get_the_ID(),
						);
					
					$quote_query = new WP_Query( $args ); ?>

		</div><!-- #property-main-content -->

		<div id="property-side-content">

				<?php if ( $quote_query->have_posts() ) : while ( $quote_query->have_posts() ) : $quote_query->the_post(); ?>

				<div class="property-testimonial">
				
					<blockquote class="quote"><?php the_content(); ?></blockquote>

					<div class="info">
						<span class="name"><?php the_title(); ?></span>
						<?php $location = get_post_meta( get_the_ID(), 'pwtestimonial_location', true ); ?>
						<?php if ( $location ) : ?>
							<span> - </span>
							<span class="location"><?php echo esc_html( $location ); ?></span>
						<?php endif; ?>
						<br>
						<strong class="where"><?php echo get_the_title( $post_id ); ?> Guest</strong>
					</div>

					<a href="<?php echo get_the_permalink( get_option( 'pw_testimonials_page', false ) ); ?>">View All Testimonials</a>

				</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			<hr class="sep" />

			<div class="buttons">
				
				<?php $rates_desc = get_post_meta( get_the_ID(), 'pwproperty_rates_description', true );
					if ( ! empty( trim( $rates_desc ) ) ) : ?>
						<a href="#rates-popup" class="rates button open-popup-link">Request Rates &amp; Info</a>

						<br />
				<?php endif; ?>

				<?php $cal = get_post_meta( get_the_ID(), 'pwproperty_availability', true ); ?>
				<?php if ( $cal ) : ?>
					<a href="#availability-popup" class="availability button open-popup-link">Availability</a>
					<br />
				<?php endif; ?>

				<a href="#resources-popup" class="resources button open-popup-link">Floor Plans &amp; Details</a>
				
				<!-- <?php $gallery_link = get_post_meta( get_the_ID(), 'pwproperty_photo_gallery_link', true ); ?>
				<?php if ( $gallery_link ) : ?>
					<?php $gallery_link = get_term_link( (int) $gallery_link, 'media_category' ); ?>
					<br />
					<a href="<?php echo $gallery_link; ?>" class="gallery button">Photo Gallery</a>
				<?php endif; ?> -->
			</div>

			<div class="property-map clear">
				<?php $map = get_post_meta( get_the_ID(), 'pwproperty_map', true ); ?>
				<?php $address = trim( get_post_meta( get_the_ID(), 'pwproperty_address', true ) ); ?>
				<a href="#full-map-popup" class="open-popup-link"><img src="<?php echo $map; ?>" /></a>
				<div class="inner">
					<address class="address">
					<?php echo esc_html( $address ); ?><br />
					Manhattan, Kansas 66503
					</address>
					<div class="hide-for-print"><a href="<?php echo 'https://google.com/maps?q=' . urlencode( esc_html( $address ) . ' Manhattan, Kansas 66503' ); ?>" target="_blank">Directions</a> or <a href='#full-map-popup' target="_blank" class="open-popup-link">View Full Map</a></div>
				</div>
			</div>

		</div><!-- #property-side-content -->

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'prairiewood' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->

	<div id="rates-popup" class="mfp-hide magnific">
		<div class="inner">
			<h2 class="title gold-color-text">Rates for <?php the_title(); ?></h2>

			<?php $rates_desc = get_post_meta( get_the_ID(), 'pwproperty_rates_description', true );
				if ( ! empty( trim( $rates_desc ) ) ) : ?>
					
					<p><?php echo apply_filters('the_content', $rates_desc ); ?></p>

			<!-- <?php endif;

				$rates = get_post_meta( get_the_ID(), 'pwproperty_rates', true );
				if ( ! empty( $rates ) ) : ?>

					<table class="rates-table">
						<thead>
							<tr>
								<td>Days/Nights</td>
								<td>Weekdays</td>
								<td>Weekends</td>
							</tr>
						</thead>
						<tbody>
							<?php
								

								 foreach ( $rates as $key => $rate ) : ?>
									<tr>
										<td><?php echo $rate['days']; ?></td>
										<td><?php echo $rate['weekdays']; ?></td>
										<td><?php echo $rate['weekends']; ?></td>
									</tr>
								<?php endforeach; ?>
						</tbody>
					</table>
			<?php else : ?>
				<p>Rates are not currently available for this property. Please reach out to us directly for more information.</p>
				<a href="/contact" class="button">Contact Us</a>
			<?php endif; ?>

			<?php $add_rates_desc = get_post_meta( get_the_ID(), 'pwproperty_additional_rates_description', true );
				if ( ! empty( trim( $add_rates_desc ) ) ) : ?>
					
					<p><?php echo wpautop( $add_rates_desc ); ?></p>

			<?php endif;

				$additional_rates = get_post_meta( get_the_ID(), 'pwproperty_additional_rates', true );
				if ( ! empty( $additional_rates ) ) : ?>

					<table class="rates-table">
						<thead>
							<tr>
								<td>Days/Nights</td>
								<td>Weekdays</td>
								<td>Weekends</td>
							</tr>
						</thead>
						<tbody>
							<?php
								
								 foreach ( $additional_rates as $key => $rate ) : ?>
									<tr>
										<td><?php echo $rate['days']; ?></td>
										<td><?php echo $rate['weekdays']; ?></td>
										<td><?php echo $rate['weekends']; ?></td>
									</tr>
								<?php endforeach; ?>
								
						</tbody>
					</table>
			<?php endif; ?> -->
		</div><!-- inner -->
	</div><!-- Rates Popup -->
	
	<div id="availability-popup" class="mfp-hide magnific">
		<div class="inner">
			<h2 class="title gold-color-text"> <?php the_title(); ?> Availability Calendar </h2>
			<ul class="key">
				<li class="square red"><span>Reserved</span></li>
				<li class="square green"><span>Today</span></li>
			</ul>
			<?php echo do_shortcode("[calendar id='{$cal}']"); ?>
		</div>
	</div>

	<div id="book-now-popup" class="mfp-hide magnific">
		<div class="inner">
			<?php $book_now = get_post_meta( get_the_ID(), 'pwproperty_book_now', true );
				if ( ! empty( trim( $book_now ) ) ) : ?>
					
					<p><?php echo apply_filters( 'the_content', $book_now ); ?></p>

			<?php endif; ?>
		</div>
	</div>

	<div id="resources-popup" class="mfp-hide magnific">
		<div class="inner">
			<?php 

				$form_id = get_post_meta( get_the_ID(), 'pwproperty_resource_form', true );

				if ( function_exists('gravity_form' ) ) :
					gravity_form( $form_id, false, false, false, null, true );
				endif;

			?>
		</div>
	</div>

	<div id="full-map-popup" class="mfp-hide magnific">
		<?php $pw_full_map = get_post_meta( get_the_ID(), 'pwproperty_map_full', true ); ?>
		<img src="<?php echo $pw_full_map; ?>" />
	</div>

	<div class="property-meta">
		<div class="content-wrapper">
			
			<div class="facilities"><!--
				<?php 
					$bedrooms = trim( get_post_meta( get_the_ID(), 'pwproperty_bedrooms', true ) );
					
					$bathrooms = trim( get_post_meta( get_the_ID(), 'pwproperty_bathrooms', true ) );
					
					if ( $bedrooms ){
						echo "<span class='fa fa-bed facility' title='Bedrooms'> <span class='font-main'>{$bedrooms}</span> <span class='screen-reader-text'>Bedrooms</span></span>";
					}

					if ( $bathrooms ){
						echo "<span class='fa fa-tint facility' title='Bathrooms'> <span class='font-main'>{$bathrooms}</span> <span class='screen-reader-text'>Bathrooms</span></span>";
					}
				?>
			--></div>

			<div class="social">
				
				<?php $book_now = get_post_meta( get_the_ID(), 'pwproperty_book_now', true );
					if ( ! empty( trim( $book_now ) ) ) : ?>
						<!-- <a href="<?php echo get_the_permalink( get_option( 'pw_contact_page', false ) ); ?>" class="book-now icon">Book Now</a> -->
						<!-- <a href="#book-now-popup" class="book-now icon open-popup-link">Book Now</a> -->
					<?php endif; ?>
				<a href="javascript:window.print();" class="icon print"><span class="fa fa-print"><span class="screen-reader-text">Print this page</span></span></a>
				<a href="<?php the_permalink(); ?>" class="icon link"><span class="fa fa-link"><span class="screen-reader-text">Link to this page</span></span></a>
				<a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&description=<?php echo urlencode(get_the_title());?>&media=<?php echo $bg_image[0]; ?>" target="_blank" class="icon pinterest"><span class="fa fa-pinterest"><span class="screen-reader-text">Pin this page on Pinterest</span></span></a>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" class="icon facebook"><span class="fa fa-facebook-square"><span class="screen-reader-text">Share this page on Facebook</span></span></a>
			</div>

		</div>
	
	</div>

</article><!-- #post-## -->