<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

?>
<!-- Start content-page.php -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
		<div class="title-wrap">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<!-- <div class="sub-title">at Prairiewood</div> -->
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content content-wrapper">

		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'prairiewood' ),
				'after'  => '</div>',
			) );
		?>
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'prairiewood' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<!-- End content-page.php -->