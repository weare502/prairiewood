<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Prairiewood
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function prairiewood_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'prairiewood_body_classes' );

/**
 * Get all pages to use in custom loop
 *
 * @return array Array of all pages.
 */
function pw_get_pages() {
	$pages = new WP_Query( array(
		'post_type' => 'page',
		'posts_per_page' => -1,
	) );

	$new_arr = array(
		'' => 'None',
	);

	foreach ( $pages->posts as $page ) {
		$new_arr[ $page->ID ] = $page->post_title;
	}

	return $new_arr;

}
