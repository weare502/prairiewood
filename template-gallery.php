<?php
/**
 * Template Name: Gallery Template
 */
get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
					<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
						<div class="title-wrap">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<!-- <div class="sub-title">at Prairiewood</div> -->
						</div>
					</header><!-- .entry-header -->

					<div class="entry-content content-wrapper">

						<?php the_content(); ?>
						
						<div class="gallery-grid">
						<?php $galleries = get_terms( 'media_category' );
							if ( $galleries ) :
								foreach ( $galleries as $gallery ) : ?>
								<?php $bg_image = wp_get_attachment_image_src( get_term_meta( $gallery->term_id, 'image', true ), 'full' ); ?>
									<a class="gallery-link" href="<?php echo get_term_link( $gallery ); ?>" style="background-image: url( <?php echo $bg_image[0]; ?> );">
										<div class="hover-wrapper">
											<span class="bottom-right"></span>
											<div class="gallery-title"><?php echo $gallery->name; ?></div>
										</div>
									</a>
								<?php endforeach;
							endif; 
						?>
						</div>
						
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									esc_html__( 'Edit %s', 'prairiewood' ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();