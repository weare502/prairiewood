<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prairiewood
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,400,300,700,700italic,400italic,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Crimson+Text:400,700,400italic' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'prairiewood' ); ?></a>

	<header id="masthead" class="site-header clear" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<div class="nav-wrapper">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="inner-text"><?php esc_html_e( 'Menu', 'prairiewood' ); ?></span><span class="fa fa-bars"></span></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container_id' => 'primary-menu-container', 'container_class' => 'clear' ) ); ?>
				
				<div class="social">

					<?php if ( get_option( 'pw_facebook_url' ) ) : ?>
						<a href="<?php echo get_option('pw_facebook_url'); ?>" target="_blank" class="facebook">
							<span class="fa fa-facebook"><span class="screen-reader-text">facebook</span></span>
						</a>
					<?php endif; ?>

					<?php if ( get_option( 'pw_pinterest_url' ) ) : ?>
						<a href="<?php echo get_option('pw_pinterest_url'); ?>" target="_blank" class="pinterest">
							<span class="fa fa-pinterest"><span class="screen-reader-text">pinterest</span></span>
						</a>
					<?php endif; ?>

					<?php if ( get_option( 'pw_instagram_url' ) ) : ?>
						<a href="<?php echo get_option('pw_instagram_url'); ?>" target="_blank" class="instagram">
							<span class="fa fa-instagram"><span class="screen-reader-text">instagram</span></span>
						</a>
					<?php endif; ?>


				</div><!-- .social -->


			</nav><!-- #site-navigation -->
		</div>
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
