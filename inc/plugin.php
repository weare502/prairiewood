<?php
/**
 * Plugin Name: Prairiewood Features Plugin
 *
 * @package  Prairiewood
 */

/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 * @return void
 */
function register_pwdynamic() {

	$labels = array(
		'name'					=> _x( 'Dynamic Categories', 'Taxonomy plural name', 'prairiewood' ),
		'singular_name'			=> _x( 'Dynamic Category', 'Taxonomy singular name', 'prairiewood' ),
		'search_items'			=> __( 'Search Dynamic Categories', 'prairiewood' ),
		'popular_items'			=> __( 'Popular Dynamic Categories', 'prairiewood' ),
		'all_items'				=> __( 'All Dynamic Categories', 'prairiewood' ),
		'parent_item'			=> __( 'Parent Dynamic Category', 'prairiewood' ),
		'parent_item_colon'		=> __( 'Parent Dynamic Category', 'prairiewood' ),
		'edit_item'				=> __( 'Edit Dynamic Category', 'prairiewood' ),
		'update_item'			=> __( 'Update Dynamic Category', 'prairiewood' ),
		'add_new_item'			=> __( 'Add New Dynamic Category', 'prairiewood' ),
		'new_item_name'			=> __( 'New Dynamic Category Name', 'prairiewood' ),
		'add_or_remove_items'	=> __( 'Add or remove Dynamic Categories', 'prairiewood' ),
		'choose_from_most_used'	=> __( 'Choose from most used prairiewood', 'prairiewood' ),
		'menu_name'				=> __( 'Dynamic Category', 'prairiewood' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => true,
		'meta_box_cb'		=> 'pwdynamic_metabox_cb',
		'rewrite'           => array( 'slug' => __( 'experience', 'prairiewood' ) ),
		'query_var'         => true,
		'show_in_rest'      => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'pwdynamic', array( 'post', 'attachment' ), $args );

}

add_action( 'init', 'register_pwdynamic' );

function pwdynamic_metabox_cb( $post, $box ) {

	post_categories_meta_box( $post, $box );

	$obj = get_post_type_object( $post->post_type );

	if ( ! in_array( $post->post_type, array( 'pwproperty', 'pwpackage' ), true ) ) {
		return;
	}

	echo wp_kses( "<p>You'll need to update this {$obj->labels->singular_name} before the new WYSIWYG will appear below.</p>" );

}

/**
 * Registers a new post type
 *
 * @uses $wp_post_types Inserts new post type object into the list
 * @return void
 */
function register_pw_property() {

	$labels = array(
		'name'                => __( 'Properties', 'prairiewood' ),
		'singular_name'       => __( 'Property', 'prairiewood' ),
		'add_new'             => _x( 'Add New Property', 'prairiewood', 'prairiewood' ),
		'add_new_item'        => __( 'Add New Property', 'prairiewood' ),
		'edit_item'           => __( 'Edit Property', 'prairiewood' ),
		'new_item'            => __( 'New Property', 'prairiewood' ),
		'view_item'           => __( 'View Property', 'prairiewood' ),
		'search_items'        => __( 'Search Properties', 'prairiewood' ),
		'not_found'           => __( 'No Properties found', 'prairiewood' ),
		'not_found_in_trash'  => __( 'No Properties found in Trash', 'prairiewood' ),
		'parent_item_colon'   => __( 'Parent Property:', 'prairiewood' ),
		'menu_name'           => __( 'Properties', 'prairiewood' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Properties located at Prairiewood Retreat & Preserve',
		'taxonomies'          => array( 'pwdynamic' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-home',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'        => true,
		'rewrite'             => array( 'slug' => __( 'properties', 'prairiewood' ) ),
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
			'revisions',
			'custom-fields',
		),
	);

	register_post_type( 'pwproperty', $args );
}

add_action( 'init', 'register_pw_property' );

/**
 * Registers a new post type
 *
 * @uses $wp_post_types Inserts new post type object into the list.
 * @return void
 */
function register_pwtestimonial() {

	$labels = array(
		'name'                => __( 'Testimonials', 'prairiewood' ),
		'singular_name'       => __( 'Testimonial', 'prairiewood' ),
		'add_new'             => _x( 'Add New Testimonial', 'prairiewood', 'prairiewood' ),
		'add_new_item'        => __( 'Add New Testimonial', 'prairiewood' ),
		'edit_item'           => __( 'Edit Testimonial', 'prairiewood' ),
		'new_item'            => __( 'New Testimonial', 'prairiewood' ),
		'view_item'           => __( 'View Testimonial', 'prairiewood' ),
		'search_items'        => __( 'Search Testimonials', 'prairiewood' ),
		'not_found'           => __( 'No Testimonials found', 'prairiewood' ),
		'not_found_in_trash'  => __( 'No Testimonials found in Trash', 'prairiewood' ),
		'parent_item_colon'   => __( 'Parent Testimonial:', 'prairiewood' ),
		'menu_name'           => __( 'Testimonials', 'prairiewood' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'Testimonials about Prairiewood',
		'taxonomies'          => array( 'pwdynamic' ),
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-testimonial',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'show_in_rest'        => true,
		'supports'            => array(
				'title',
				'editor',
				'revisions',
			),
	);

	register_post_type( 'pwtestimonial', $args );
}

add_action( 'init', 'register_pwtestimonial' );

/**
 * Registers a new post type
 *
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function register_pwpackage() {

	$labels = array(
		'name'                => __( 'Packages', 'prairiewood' ),
		'singular_name'       => __( 'Package', 'prairiewood' ),
		'add_new'             => _x( 'Add New Package', 'prairiewood', 'prairiewood' ),
		'add_new_item'        => __( 'Add New Package', 'prairiewood' ),
		'edit_item'           => __( 'Edit Package', 'prairiewood' ),
		'new_item'            => __( 'New Package', 'prairiewood' ),
		'view_item'           => __( 'View Package', 'prairiewood' ),
		'search_items'        => __( 'Search Packages', 'prairiewood' ),
		'not_found'           => __( 'No Packages found', 'prairiewood' ),
		'not_found_in_trash'  => __( 'No Packages found in Trash', 'prairiewood' ),
		'parent_item_colon'   => __( 'Parent Package:', 'prairiewood' ),
		'menu_name'           => __( 'Packages', 'prairiewood' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Packages available at Prairiewood',
		'taxonomies'          => array( 'pwdynamic' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-products',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array( 'slug' => __( 'packages', 'prairiewood' ) ),
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
	'editor',
	'thumbnail',
	'revisions',
			),
	);

	register_post_type( 'pwpackage', $args );
}

add_action( 'init', 'register_pwpackage' );


/**
 * Registers a new post type
 *
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function register_pwdynamicblock() {

	$labels = array(
		'name'                => __( 'Blocks', 'prairiewood' ),
		'singular_name'       => __( 'Block', 'prairiewood' ),
		'add_new'             => _x( 'Add New Block', 'prairiewood', 'prairiewood' ),
		'add_new_item'        => __( 'Add New Block', 'prairiewood' ),
		'edit_item'           => __( 'Edit Block', 'prairiewood' ),
		'new_item'            => __( 'New Block', 'prairiewood' ),
		'view_item'           => __( 'View Block', 'prairiewood' ),
		'search_items'        => __( 'Search Blocks', 'prairiewood' ),
		'not_found'           => __( 'No Blocks found', 'prairiewood' ),
		'not_found_in_trash'  => __( 'No Blocks found in Trash', 'prairiewood' ),
		'parent_item_colon'   => __( 'Parent Block:', 'prairiewood' ),
		'menu_name'           => __( 'Blocks', 'prairiewood' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'Dynamic Page Blocks',
		'taxonomies'          => array( 'pwdynamic' ),
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-layout',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'show_in_rest'        => true,
		'supports'            => array(
			'title',
	'editor',
	'thumbnail',
			),
	);

	register_post_type( 'pwdynamicblock', $args );
}

add_action( 'init', 'register_pwdynamicblock' );

function cmbproperty_availability_options( $field ) {
	$cals = get_posts( array( 'post_type' => 'calendar', 'posts_per_page' => -1 ) );

	if ( ! $cals ) {
		return false;
	}
	$calendars = array();
	foreach ( $cals as $cal ) {
		$calendars[ $cal->ID ] = $cal->post_title;
	}
	return $calendars;
}

function cmbproperty_photo_gallery( $field ) {
	$taxonomies = get_terms( 'media_category', array(
		'hide_empty' => false,
	) );

	if ( ! $taxonomies ) {
		return false; }

	$taxes = array();
	foreach ( $taxonomies as $term ) {
		$taxes[ $term->term_id ] = $term->name;
	}

	return $taxes;
}

function cmbproperty_get_forms( $field ) {
	if ( ! class_exists( 'GFAPI' ) ) { return array( 'Gravity Forms Not Installed' ); }

	$forms = GFAPI::get_forms();

	$forms_list = array();

	foreach ( $forms as $form ) {
		$forms_list[ $form['id'] ] = $form['title'];
	}

	return $forms_list;
}

function cmb2_pwproperty_metaboxes() {

	$prefix = 'pwproperty_';

	$cmb_property = new_cmb2_box( array(
		'id'          	=> 'pwproperty_info',
		'title'       	=> __( 'Property Info', 'prairiewood' ),
		'object_types'	=> array( 'pwproperty' ), // Post type
		'context'     	=> 'normal',
		'priority'    	=> 'high',
		'show_names'  	=> true, // Show field names on the left
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Bedrooms' ),
		'id' => $prefix . 'bedrooms',
		'type' => 'text_small',
		'default' => 0,
		'attributes' => array(
				'type' => 'number',
				'step' => '.5',
			),
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Bathrooms' ),
		'id' => $prefix . 'bathrooms',
		'type' => 'text_small',
		'default' => 0,
		'attributes' => array(
				'type' => 'number',
				'step' => '.5',
			),
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Street Address' ),
		'id' => $prefix . 'address',
		'type' => 'text_medium',
		'default' => '',
		'description' => 'Just the street address, no city/state/zip.',
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Thumbnail Map Image' ),
		'id' => $prefix . 'map',
		'type' => 'file',
		'description' => 'Upload/Choose the map for this property.',
		'options' => array( 'url' => false ),
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Full Map Image' ),
		'id' => $prefix . 'map_full',
		'type' => 'file',
		'description' => 'Upload/Choose the full size map for this property.',
		'options' => array( 'url' => false ),
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Key Features' ),
		'id' => $prefix . 'key_features',
		'type' => 'text_medium',
		'default' => '',
		'description' => 'Keep them short! Try to <b>limit to 4 or 5.</b>',
		'repeatable' => true,
		'options' => array( 'add_row_text' => 'Add Key Feature' ),
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Amenities' ),
		'id' => $prefix . 'amenities',
		'type' => 'text',
		'default' => '',
		'description' => 'Keep them short, but a little longer than Key Features.',
		'repeatable' => true,
		'options' => array( 'add_row_text' => 'Add Amenity' ),
	) );

	// $cmb_property->add_field( array(
	// 'name' => __( 'Rates' ),
	// 'id' => $prefix . 'rates',
	// 'type' => 'file',
	// 'description' => 'Upload the rate card PDF for this property.',
	// 'options' => array( 'url' => false )
	// ) );
	$cmb_property->add_field( array(
		'name' => __( 'Image Gallery' ),
		'id' => $prefix . 'image_gallery',
		'type' => 'file_list',
		'description' => 'Upload/choose images of this property for the gallery. You can select multiple images from the Media Uploader.',
		'options' => array( 'url' => false ),
	) );

	$cmb_property->add_field( array(
		'name'    => __( 'Photo Gallery', 'cmb2' ),
		'desc'    => __( 'This will generate a link to a gallery of all the photos tagged for this property. This list is automatically populated with all of the "Media Categories" from the media library.', 'cmb2' ),
		'id'      => $prefix . 'photo_gallery_link',
		'type'    => 'select',
		'show_option_none' => true,
		'options_cb' => 'cmbproperty_photo_gallery',
	) );

	$cmb_property->add_field( array(
		'name'    => __( 'Availability Calendar', 'cmb2' ),
		'desc'    => __( 'Choose the calendar associated with this property.', 'cmb2' ),
		'id'      => $prefix . 'availability',
		'type'    => 'select',
		'show_option_none' => true,
		'options_cb' => 'cmbproperty_availability_options',
	) );

	$cmb_property->add_field( array(
		'name'    => __( 'Resources Form', 'cmb2' ),
		'desc'    => __( 'Choose the form to gather info from visitors re: Resources.', 'cmb2' ),
		'id'      => $prefix . 'resource_form',
		'type'    => 'select',
		'show_option_none' => true,
		'options_cb' => 'cmbproperty_get_forms',
	) );

	$cmb_property->add_field( array(
		'name' => __( 'Rates Description' ),
		'id' => $prefix . 'rates_description',
		'type' => 'wysiwyg',
		'default' => '',
		'description' => 'A place to enter the rates for this property. Use TablePress to add rate tables. Leave this empty is you wish to omit rates from this property.',
		'attributes' => array(
			'rows' => 3,
		),
	) );

	 $cmb_property->add_field( array(
		 'name' => __( 'Book Now Description' ),
		 'id' => $prefix . 'book_now',
		 'type' => 'wysiwyg',
		 'default' => '',
		 'description' => 'A place to enter the book now info for this property. Use TablePress to add rate tables. Leave this empty is you wish to omit BOOK NOW from this property.',
		 'attributes' => array(
			'rows' => 3,
		 ),
	 ) );

	 /*
	 @TODO: decide about rates

	 $cmb_property->add_field( array(
    	'name' => __( 'Rates' ),
    	'id' => $prefix . 'rates',
    	'type' => 'group',
    	'repeatable' => true,
    	'options' => array(
    		'group_title' => __( 'Rate #{#}', 'prairiewood' ),
    		'add_button' => __( 'Add another Rate', 'prairiewood' ),
    		'remove_button' => __( 'Remove Rate', 'prairiewood' ),
    		'sortable' => true,
    		'closed' => true,
    	),
	 ) );

	 $cmb_property->add_group_field( $prefix . 'rates', array(
    	'name' => 'Days/Nights',
    	'id' => 'days',
    	'type' => 'text_small',
	 ) );

	 $cmb_property->add_group_field( $prefix . 'rates', array(
    	'name' => 'Weekdays',
    	'id' => 'weekdays',
    	'type' => 'text_small',
	 ) );

	 $cmb_property->add_group_field( $prefix . 'rates', array(
    	'name' => 'Weekends',
    	'id' => 'weekends',
    	'type' => 'text_medium',
	 ) );

	 $cmb_property->add_field( array(
    	'name' => __( 'Additional Rates Description' ),
    	'id' => $prefix . 'additional_rates_description',
    	'type' => 'textarea',
    	'default' => '',
    	'description' => 'A short-ish description describing the below rates.',
    	'attributes' => array(
    		'rows' => 3
    	),
	 ) );

	 $cmb_property->add_field( array(
    	'name' => __( 'Additional Rates' ),
    	'id' => $prefix . 'additional_rates',
    	'description' => 'This should be used SPARINGLY. For things like winter rates. Most properties don\'t need this',
    	'type' => 'group',
    	'repeatable' => true,
    	'options' => array(
    		'group_title' => __( 'Rate #{#}', 'prairiewood' ),
    		'add_button' => __( 'Add another Rate', 'prairiewood' ),
    		'remove_button' => __( 'Remove Rate', 'prairiewood' ),
    		'sortable' => true,
    		'closed' => true,
    	),
	 ) );

	 $cmb_property->add_group_field( $prefix . 'additional_rates', array(
    	'name' => 'Days/Nights',
    	'id' => 'days',
    	'type' => 'text_small',
	 ) );

	 $cmb_property->add_group_field( $prefix . 'additional_rates', array(
    	'name' => 'Weekdays',
    	'id' => 'weekdays',
    	'type' => 'text_small',
	 ) );

	 $cmb_property->add_group_field( $prefix . 'additional_rates', array(
    	'name' => 'Weekends',
    	'id' => 'weekends',
    	'type' => 'text_medium',
	 ) );

	 */

	 //
	 /*
	 ADD THINGS HERE ABOVE HERE */
	 //
	 if ( isset( $_REQUEST['post'] ) || isset( $_REQUEST['post_ID'] ) ) :

		 $postID = isset( $_REQUEST['post'] ) ? intval( $_REQUEST['post'] ) : false;
		 $post_id = isset( $_REQUEST['post_ID'] ) ? intval( $_REQUEST['post_ID'] ) : false;

		 if ( $postID ) {
			 $post = $postID; } elseif ( $post_id ) {
				$post = $post_id;
				} else { 			$post = 0; }

				// $post = intval( $_REQUEST['post'] );
				$terms = wp_get_post_terms( $post, 'pwdynamic' );

				if ( $terms ) :

					/**
			 * Initiate the metabox
			 */
					$cmb_dynamic = new_cmb2_box( array(
						'id'          	=> 'pwdynamic_' . get_post_type( $post ),
						'title'       	=> __( 'Dynamic Page Info', 'cmb2' ),
						'object_types'	=> array( 'pwproperty', 'pwpackage' ), // Post type
						'context'     	=> 'normal',
						'priority'    	=> 'low',
						'show_names'  	=> true, // Show field names on the left
					) );

					foreach ( $terms as $term ) {
						$cmb_dynamic->add_field( array(
							'name'      => __( $term->name . ' WYSIWYG', 'cmb2' ),
							// generate our own "prefix" based on the post type just in case
							'id'        => get_post_type( $post ) . '_' . 'dynamic_' . $term->term_id,
							'type'      => 'wysiwyg',
							'options'	=> array(),
						) );

						$cmb_dynamic->add_field( array(
							'name'      => __( $term->name . ' Dynamic Page Image', 'cmb2' ),
							// generate our own "prefix" based on the post type just in case
							'id'        => get_post_type( $post ) . '_' . 'dynamic_image_' . $term->term_id,
							'type'      => 'file',
							'options' => array( 'url' => false ),
						) );
					}

		  endif;

	endif;

}
add_action( 'cmb2_init', 'cmb2_pwproperty_metaboxes' );

add_action('cmb2_init', function(){
		$cmb_package = new_cmb2_box( array(
			'id'          	=> 'pwpackage_info',
			'title'       	=> __( 'Extra Package Info', 'prairiewood' ),
			'object_types'	=> array( 'pwpackage' ), // Post type
			'context'     	=> 'normal',
			// 'priority'    	=> 'high',
			'show_names'  	=> true, // Show field names on the left
		) );

		$cmb_package->add_field( array(
			'name'    => __( 'Resources Form', 'cmb2' ),
			'desc'    => __( 'Choose the form to gather info from visitors re: Resources.', 'cmb2' ),
			'id'      => 'pwpackage_' . 'resource_form',
			'type'    => 'select',
			'show_option_none' => true,
			'options_cb' => 'cmbproperty_get_forms',
		) );

		$cmb_package->add_field( array(
			'name'    => __( 'Resources Intro Paragraph', 'cmb2' ),
			'desc'    => __( 'Use <code>&lt;br/&gt;</code> for line breaks.', 'cmb2' ),
			'id'      => 'pwpackage_' . 'resource_intro',
			'type'    => 'textarea',
			// 'show_option_none' => true,
			// 'options_cb' => 'cmbproperty_get_forms',
			'attributes' => array(
				'rows' => 3,
			),
		) );
} );

// add_action( 'save_post', function(){
// global $post;
// if ( $post->post_type !== 'pwproperty' )
// return;
// foreach ( $_REQUEST as $key => $value ){
// $start = strpos( $key, 'pwproperty_dynamic_' );
// if ( $start !== false ){
// update_post_meta( $post->ID, $key, wp_kses_post( $value ) );
// wp_die('Found one!');
// }
// }
// });
function cmb2_pwtestimonial_metaboxes() {

	$prefix = 'pwtestimonial_';

	$cmb_testimonial = new_cmb2_box( array(
		'id'          	=> 'pwtestimonial_info',
		'title'       	=> __( 'Testimonial Info', 'prairiewood' ),
		'object_types'	=> array( 'pwtestimonial' ), // Post type
		'context'     	=> 'normal',
		'priority'    	=> 'high',
		'show_names'  	=> true, // Show field names on the left
	) );

	$cmb_testimonial->add_field( array(
		'name' => __( 'Location' ),
		'id' => $prefix . 'location',
		'type' => 'text_medium',
		'default' => '',
		'description' => 'Usually a city and state.',
	) );

}

add_action( 'cmb2_init', 'cmb2_pwtestimonial_metaboxes' );


// Callback function to insert 'styleselect' into the $buttons array
function tiak_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	// var_dump($buttons);
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'tiak_mce_buttons' );

// Callback function to filter the MCE settings
function tiak_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Button',
			'selector' => 'a',
			'classes' => 'button',
			'icon'	   => ' fa fa-hand-pointer-o',
		),
		// array(
		// 'title' => 'Horizontal Rule',
		// 'selector' => 'p',
		// 'block' => 'hr',
		// 'classes' => 'hr',
		// 'icon'     => ' fa fa-ellipsis-h'
		// ),
		// array(
		// 'title' => 'H.R. Primary Color',
		// 'selector' => 'hr',
		// 'classes' => 'hr primary-color-hr',
		// 'icon'      => ' fa fa-ellipsis-h'
		// ),
		// array(
		// 'title' => 'Full Width',
		// 'selector' => 'p',
		// 'classes' => 'full-width',
		// 'icon'     => ' fa fa-arrows-h'
		// ),
		// array(
		// 'title' => 'Primary Color Background',
		// 'selector' => 'p',
		// 'classes' => 'primary-color',
		// 'icon'      => ' fa fa-circle-thin'
		// ),
		array(
			'title' => 'Light Gray Background',
			'selector' => 'p, div, h1, h2, h3, h4, h5, h6',
			'classes' => 'light-gray-bg',
			// 'icon'	   => ' fa fa-circle-thin'
		),
		// array(
		// 'title' => 'Larger Text',
		// 'selector' => '*',
		// 'classes' => 'larger',
		// 'icon'     => ' fa fa-text-height'
		// ),
		array(
			'title' => 'Gold Text',
			'selector' => '*',
			'classes' => 'gold-color-text',
			'icon'	   => ' fa fa-eyedropper',
		),
		// array(
		// 'title' => 'Document Icon',
		// 'selector' => 'a',
		// 'classes' => 'document-icon',
		// 'icon'     => ' fa fa-file-text'
		// ),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'.
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init' .
add_filter( 'tiny_mce_before_init', 'tiak_mce_before_init_insert_formats' );


add_action('admin_footer', function() {
	if ( ! isset( $_REQUEST['post'] ) ) {
		return;
	} ?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.cmb-field-list').sortable();
		});
	</script>
	<style type="text/css">
		.cmb-repeat .cmb-row {
			cursor: move;
		}

		.cmb-row.ui-sortable-placeholder {
			visibility: visible !important;
			border: 1px dashed black;
		}

		.cmb-row.ui-sortable-helper {
			background-color: lightgreen;
		}

		.cmb-repeat-group-wrap .cmb2-wrap>.cmb-field-list>.cmb-row, .postbox-container .cmb2-wrap>.cmb-field-list>.cmb-row {
			padding: 1em 0;
		}
	</style>
<?php });

add_filter( 'simcal_calendar_class', function( array $classes ) {

	foreach ( $classes as $key => $value ) {
		if ( strpos( $value, 'calendar-light' ) !== false || strpos( $value, 'calendar-dark' ) !== false ) {
			unset( $classes[ $key ] );
		}
	}
	return $classes;
}, 20 ); // After class is added.

add_filter( 'tablepress_table_template', function( $data ) {

	// Turn off DataTables JS functionality by default.
	$data['options']['use_datatables'] = false;

	return $data;

}, 10 );

add_action( 'wp_enqueue_scripts', function(){
	// Remove TablePress CSS.
	wp_dequeue_style( 'tablepress-default' );
}, 100 );

add_filter( 'tablepress_table_css_classes', function( $classes ) {

	// Add custom CSS class to data tables.
	$classes[] = 'rates-table';

	return $classes;
}, 100 );


add_action( 'current_screen', function(){

	$screen = get_current_screen();

	if ( 'post' === $screen->base  ) {
		unregister_widget( 'Envira_Albums_Widget' );
	}
}, 100 );
