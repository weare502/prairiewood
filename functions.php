<?php
/**
 * Prairiewood functions and definitions
 *
 * @category Theme File
 * @author Daron Spence <daron@502mediagroup.com>
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package Prairiewood
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @since 1.0
 */

if ( ! function_exists( 'prairiewood_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function prairiewood_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Prairiewood, use a find and replace
		 * to change 'prairiewood' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'prairiewood', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'prairiewood' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_editor_style();

	}
endif;
add_action( 'after_setup_theme', 'prairiewood_setup' );

require get_template_directory() . '/inc/plugin.php';

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function prairiewood_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'prairiewood_content_width', 640 );
}
add_action( 'after_setup_theme', 'prairiewood_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function prairiewood_scripts() {

	wp_enqueue_style( 'prairiewood-style', get_stylesheet_uri(), array(), '20160626' );

	if ( is_singular( 'pwproperty' ) ) {
		// wp_enqueue_style( 'prairiewood-galleria', '/galleria/themes/classic/galleria.classic.css', array() );
	}

	wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array( 'prairiewood-style' ), '20120206' );

	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'velocity-js', get_template_directory_uri() . '/bower_components/velocity/velocity.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'prairiewood-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery', 'magnific-popup' ), '20120215', true );

	wp_enqueue_script( 'prairiewood-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'fitvids-js', 'https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js', array( 'jquery', 'prairiewood-navigation' ), '1.1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_front_page() ) {
		wp_enqueue_script( 'vide-js', get_template_directory_uri() . '/bower_components/vide/dist/jquery.vide.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120206', true  );
		wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css' );
	}

	if ( is_tax( 'pwdynamic' ) ) {
		wp_enqueue_script( 'prairiewood-dynamic', get_template_directory_uri() . '/js/dynamic.js', array( 'jquery', 'underscore', 'velocity-js', 'fitvids-js' ), '20100105', true );
	}
}
add_action( 'wp_enqueue_scripts', 'prairiewood_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Returns index of the end of an array
 *
 * @param  array $array Input array.
 * @return int Index of end of $array.
 */
function endc( $array ) {
	return end( $array );
}

/**
 * Sort an array in a psuedo random order.
 *
 * @param  array $arr Input array.
 * @return array Sorted array.
 */
function sort_array_random_separate( $arr ) {
	$old = $arr;
	$new = array();
	$new[] = array_pop( $old );
	$loopcount = count( $old );

	for ( $i = 0; $i < $loopcount; $i++ ) {
			shuffle( $old );
		$new[] = $old[0];
		array_shift( $old );
	}
	return $new;
}

/**
 * Template tag to output navigation with pagination.
 *
 * @return void
 */
function pw_pagination_bar() {
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;

	if ( $total_pages > 1 ) {
		$current_page = max( 1, get_query_var( 'paged' ) );

		echo esc_html( paginate_links( array(
			'base' => get_pagenum_link( 1 ) . '%_%',
			'format' => '/page/%#%',
			'current' => $current_page,
			'total' => $total_pages,
		) ) );
	}
}
