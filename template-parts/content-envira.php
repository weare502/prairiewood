<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

?>
<!-- Start content.php -->

<?php $term = $wp_query->get_queried_object(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $bg_image = wp_get_attachment_image_src( get_term_meta( $term->term_id, 'image', true ), 'full' ); ?>
	<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
		<div class="title-wrap">
			<h1 class="entry-title"><?php echo $term->name; ?></h1>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php prairiewood_posted_on(); ?>
				<?php prairiewood_entry_footer(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
			
			
			$gallery_imgs = new WP_Query( array( 
					//Type & Status Parameters
					'post_type'   => 'attachment',
					// Attachments don't have a status
					'post_status' => 'any',
					//Order & Orderby Parameters
					'orderby'     => 'menu_order',
					'order' 	  => 'ASC',
					//Pagination Parameters
					'posts_per_page'         => -1,
					//Taxonomy Parameters
					'tax_query' => array(
						array(
							'taxonomy'         => 'media_category',
							'terms'            => array( $term->term_id ),
							'operator'         => 'IN'
						),
					),
				)
			);

			if ( $gallery_imgs->have_posts() ) :
				$gallery_ids = array();

				foreach ($gallery_imgs->posts as $img ) {
					$gallery_ids[] = $img->ID;
				}
				
				$gallery_ids_string = implode(',', $gallery_ids);

				// var_dump( envira_dynamic( array( 'id' => 'custom-thing', 'images' => '257' ) ) );

				envira_dynamic( array( 'id' => 'custom-media-cats', 'images' => $gallery_ids_string ) );
			endif;

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'prairiewood' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php // prairiewood_entry_footer(); ?>
		<div class="seperator"></div>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<!-- End content.php -->