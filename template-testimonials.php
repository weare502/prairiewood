<?php 
/**
 * Template Name: Testimonials Template
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				
				<!-- Start content-page.php -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
					<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
						<div class="title-wrap">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<!-- <div class="sub-title">at Prairiewood</div> -->
						</div>
					</header><!-- .entry-header -->

					<div class="entry-content content-wrapper">

						<?php
							the_content();

							$testimonials = new WP_Query( array(
								'post_type' => 'pwtestimonial',
								'posts_per_page' => -1
							) );

							if ( $testimonials->have_posts() ) : ?>
								
								<div id="testimonials-list">

								<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
									
									<div class="testimonial">
										<blockquote>
											<div><?php the_content(); ?></div>
											<div>
												<h2 class="quoter"><?php the_title(); ?></h2>
												<span class="location"><?php echo get_post_meta( get_the_ID(), 'pwtestimonial_location', true ); ?></span>
											</div>
										</blockquote>
									</div>

								<?php endwhile; ?>

								</div>

							<?php endif;

							wp_reset_postdata();

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'prairiewood' ),
								'after'  => '</div>',
							) );
						?>
						
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									esc_html__( 'Edit %s', 'prairiewood' ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
				<!-- End content-page.php -->

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
