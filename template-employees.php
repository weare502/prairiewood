<?php
/**
 * Template Name: Employees Template
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<!-- Start contnet-page.php -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
					<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
						<div class="title-wrap">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<div class="sub-title">at Prairiewood</div>
						</div>
					</header><!-- .entry-header -->

					<div class="entry-content content-wrapper">

						<?php if ( have_rows( 'employees' ) ) : ?>
							<div id="employees">
							<?php while ( have_rows( 'employees' ) ) : the_row(); ?>
								<div class="employee">
									<?php $image_id = get_sub_field('image'); ?>
									<?php echo wp_get_attachment_image( $image_id, 'large' ); ?>
									<div class="info">
										<div class="name"><?php the_sub_field('name'); ?></div>
										<div class="position"><?php the_sub_field('position'); ?></div>
										<div class="bio"><?php the_sub_field('biography'); ?></div>
									</div>
								</div>
							<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									esc_html__( 'Edit %s', 'prairiewood' ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->
				<!-- End content-page.php -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();