<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Prairiewood
 */

?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<span class="site-title"><?php bloginfo( 'name' ); ?></span>

				<span class="sep"><span class="fa fa-circle"></span></span>
				<span class="address"><?php echo get_option( 'pw_address' ); ?></span>

				<span class="sep"><span class="fa fa-circle"></span></span>
				<span class="phone"><a href="tel:<?php echo get_option( 'pw_phone_number' ); ?>"><?php echo get_option( 'pw_phone_number' ); ?></a></span>

		</div><!-- .site-info -->
		<div class="agency-credit">
			<span style="visibility: hidden;">Made with <i class="fa fa-heart"></i> by <a href="http://502mediagroup.com/" rel="designer">502 Media Group</a></span>
		</div>
	</footer><!-- #colophon -->
	<div class="open-menu-overlay"></div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
