/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.
 */
( function( $ ) {
	var container, button, menu, links, subMenus, $buttonIcon, $innerText;

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		var self = this;

		// Move up through the ancestors of the current link until we hit .nav-menu.
		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {

			// On li elements toggle the class .focus.
			if ( 'li' === self.tagName.toLowerCase() ) {
				if ( -1 !== self.className.indexOf( 'focus' ) ) {
					self.className = self.className.replace( ' focus', '' );
				} else {
					self.className += ' focus';
				}
			}

			self = self.parentElement;
		}
	}

	$buttonIcon = $('.menu-toggle .fa');

	$innerText = $('.menu-toggle .inner-text');

	container = document.getElementById( 'site-navigation' );
	if ( ! container ) {
		return;
	}

	button = document.getElementsByClassName( 'menu-toggle' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}

	menu = container.getElementsByTagName( 'ul' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	menu.setAttribute( 'aria-expanded', 'false' );
	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
		menu.className += ' nav-menu';
	}

	button.onclick = function() {
		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
			container.className = container.className.replace( ' toggled', '' );
			$buttonIcon.removeClass('fa-times').addClass('fa-bars');
			$('body').removeClass('menu-toggled');
			// $innerText.toggleClass('screen-reader-text');
			button.setAttribute( 'aria-expanded', 'false' );
			menu.setAttribute( 'aria-expanded', 'false' );
		} else {
			container.className += ' toggled';
			$buttonIcon.removeClass('fa-bars').addClass('fa-times');
			$('body').addClass('menu-toggled');
			// $innerText.toggleClass('screen-reader-text');
			button.setAttribute( 'aria-expanded', 'true' );
			menu.setAttribute( 'aria-expanded', 'true' );
			// $('#page').trigger('click');
		}
	};

	// Get all the link elements within the menu.
	links    = menu.getElementsByTagName( 'a' );
	subMenus = menu.getElementsByTagName( 'ul' );

	// Set menu items with submenus to aria-haspopup="true".
	for ( var i = 0, len = subMenus.length; i < len; i++ ) {
		subMenus[i].parentNode.setAttribute( 'aria-haspopup', 'true' );
	}

	// Each time a menu link is focused or blurred, toggle focus.
	for ( i = 0, len = links.length; i < len; i++ ) {
		links[i].addEventListener( 'focus', toggleFocus, true );
		links[i].addEventListener( 'blur', toggleFocus, true );
	}

	$('#page').on('click', function(/* e */){
		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
			container.className = container.className.replace( ' toggled', '' );
			$buttonIcon.removeClass('fa-times').addClass('fa-bars');
			$('body').removeClass('menu-toggled');
			// $innerText.toggleClass('screen-reader-text');
			button.setAttribute( 'aria-expanded', 'false' );
			menu.setAttribute( 'aria-expanded', 'false' );
		}
	});

	var screenReaderText = {
		expand: "<span class='screen-reader-text'>Expand this menu item's children item</span>",
		collapse: "<span class='screen-reader-text'>Collapse this menu item's children</span>"
	};

	function initMainNavigation( container ) {
		// Add dropdown toggle that display child menu items.
		container.find( '.menu-item-has-children > a' ).after( '<button class="dropdown-toggle" aria-expanded="false">' + screenReaderText.expand + '</button>' );

		// Toggle buttons and submenu items with active children menu items.
		container.find( '.current-menu-ancestor > button' ).addClass( 'toggle-on' );
		container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

		container.find( '.menu-item-has-children').click( function(event){ 
			var _this = $(this).children('.dropdown-toggle');
			_this.toggleClass( 'toggle-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );
			_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			_this.html( _this.html() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
			event.stopPropagation();
		} );

		container.find( '.dropdown-toggle' ).click( function( event ) {
			var _this = $( this );
			event.preventDefault();
			_this.toggleClass( 'toggle-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );
			// var newHeight = $(sub).height() ? 0 : _this.next('.children, .sub-menu').attr('data-height');
			// $(sub).height( newHeight );
			_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			_this.html( _this.html() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
			event.stopPropagation();
		} );

		container.find( 'a' ).click( function( event ) {
			event.stopPropagation();
		} );
	}
	initMainNavigation( $( '.main-navigation' ) );

	$(document).ready(function(){
		var a = $('.main-navigation a[href$="#"]');
		$(a).each(function(){
			$(this).replaceWith('<span class="a">' + $(this).text() + '</span>');
		});
	});

	$('.nav-wrapper').on('click', function(e){
		// console.log(e);
		e.stopPropagation();
	});

	$('.open-popup-link').magnificPopup({
		type:'inline',
		// Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		midClick: true,
		callbacks: {
			open: function(){
				window.setTimeout(function(){
					$(window).trigger('resize');
				}, 100);
			}
		}
	});

	$(document).ready( function(){
		$('#page').fitVids();

		if ( $('body').hasClass('home') ){
			$('#mobile-slider').slick({
				dots: false,
				speed: 1000,
				autoplay: true,
				autoplaySpeed: 4000,
				fade: true,
				arrows: false,
				pauseOnHover: false,
				pauseOnFocus: false,
				swipe: false
			});
		}
	} );

} )( jQuery );
