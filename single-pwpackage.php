<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'pwpackage' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<script type="text/javascript">
	
	// (function($){

	// 	var resizeEnd;
	// 	var $w = $(window),
	// 		$wrap = $('.entry-content .content'),
	// 		$content = $('.entry-content .vertical-center-wrapper');

	// 	function resizePackageContent(){

	// 		// go back to old height calc to find out if it fits
	// 		$wrap.removeClass('too-long');

	// 		var $top = parseInt( $content.css('padding-top').slice(0, -2) ),
	// 			$bottom = parseInt( $content.css('padding-bottom').slice(0, -2) );

	// 		if ( ($content.height() + $top + $bottom + 2 ) > $wrap.height() ){
	// 			console.log('too-long');
	// 			$wrap.addClass('too-long');
	// 			return;
	// 		} else {
	// 			$wrap.removeClass('too-long');
	// 			console.log('too-short');
	// 			return;
	// 		}
	// 	}

	// 	// initiate on page load
	// 	resizePackageContent();

	// 	$w.on('resize', function() {

	// 		clearTimeout(resizeEnd);

	// 		resizeEnd = setTimeout(function() { 
	// 				resizePackageContent();
	// 		}, 200);

	// 	});

	// })(jQuery);

</script>
<?php
get_footer();
