( function( $ ){

	$('.inline').on('click', function(){
		
		if ( $(this).hasClass('open') ){
			hideOpen();
			return;
		}

		if ( $(this).hasClass('template-attachment') ){
			// don't open picture blocks
			return;
		}
		
		var width = $(window).width();
		var columns;
		
		if ( width < 500 ){
			columns = 1;
		} else if ( width < 960 ){
			columns = 2;
		} else if ( width < 1500 ){
			columns = 3;
		} else {
			columns = 4;
		}
		
		var dataTemplateId = $(this).attr('data-template');
		var templateString = $('#template-' + dataTemplateId ).html().trim();
		var template = $.parseHTML(templateString);
			$(template).attr('data-time', Date.now );
		var _this = $(this);
		var index = $('.inline').index(_this);
		var position = (index+1) % columns;
		
		if ( $('.expanded-block').length !== 0 ){
				hideOpen();
				show( index, columns, position, template, _this );
		} else {
			show( index, columns, position, template, _this );
		}
		
		$(this).addClass('open').attr('data-time', Date.now );
		
	});

	function hideOpen(){
		var	$blocks = $('.expanded-block');
		var order = [];
		var $inline = $('.inline.open');
		
		$blocks.each(function(i){
			order[i] = $(this).attr('data-time');
		});
		order.sort();
		$(".expanded-block[data-time='" + order[0] + "']").slideUp(400, function(){
			$(this).remove();
		});

		order = [];
		
		$inline.each(function(i){
			order[i] = $(this).attr('data-time');
		});
		order.sort();
		var $el = $(".inline[data-time='" + order[0] + "']").removeClass('open').addClass('closed');
		
		window.setTimeout( function(){
			$el.removeClass('closed');
		}, 1000 );

	} // end hideOpen();

	function show( index, columns, position, template, _this ){
		var newBlock;

		if ( 0 === position ){ // end of row;
			_this.after(template);
			newBlock = $('.expanded-block[data-time=' + $(template).attr('data-time') + ']');
			newBlock.slideDown(400);
			
		} else {
			var nextRow = $('.inline:eq( ' + ( (columns-position) + index) + ' )');

			if ( nextRow.length ){
				nextRow.after(template);
				newBlock = $('.expanded-block[data-time=' + $(template).attr('data-time') + ']');
				newBlock.slideDown(400);
			} else {
				$('.inline').last().after(template);
				newBlock = $('.expanded-block[data-time=' + $(template).attr('data-time') + ']');
				newBlock.slideDown(400);
			}
		}

		// Wait for Sliding Animations to finish and then scroll into view.
		window.setTimeout(function(){
			$('html, body').animate({
				scrollTop: ( $(newBlock).offset().top - 16 )
			}, 500);
		}, 450 );
		
		// Resize videos if they exist in the new block
		$('#page').fitVids();
		
	} // end show(); 



	/*#############################
	RESIZING FUNCTIONS
	#############################*/


	var resizeEnd;
	var windowWidth = $(window).width();

	$(window).on('resize', function() {

		clearTimeout(resizeEnd);

		resizeEnd = setTimeout(function() { 
				$(window).trigger('resizeEnd');
		}, 100);

	});

	$(window).on('resizeEnd', function(){
		if ( $('.inline.open').length === 0 ){
			return;
		}
		var width = $(window).width();
		// var columns;

		if ( width === windowWidth ){
			return;
		}
		
		// if ( width < 500 ){
		// 	columns = 1;
		// } else if ( width < 960 ){
		// 	columns = 2;
		// } else if ( width < 1500 ){
		// 	columns = 3;
		// } else {
		// 	columns = 4;
		// }
		
		// var $open = $('.inline.open');
		// var index = $open.index();
		// var dataTemplateId = $open.attr('data-template');
		// var template = $('#template-' + dataTemplateId ).html();
		// var $this = $open;
		// var position = (index+1) % columns;
		
		windowWidth = width;
		hideOpen();
	/*	
		show( index, columns, position, template, $this );
		$this.addClass('open');*/
		
	});

})( jQuery );