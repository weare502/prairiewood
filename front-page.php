<?php
/**
 * Front Page Template
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>			

				<div id="video-container" data-vide-bg="mp4: <?php echo get_template_directory_uri(); ?>/img/ocean" data-vide-options="className: vide, posterType: none" style="background-position: 50% 50%; background-repeat: no-repeat no-repeat; background-size: cover; position: relative;">
					<div id="call-to-action">
						<?php echo esc_html( get_field( 'home_call_to_action' ) ); ?>
						<!-- <div class="tiny-text">Discover the Possibilities</div> -->
					</div>
					<a href="#full-video-modal" class="open-popup-link full-video-modal">View Video &nbsp;<span class="fa fa-volume-up"></span></a>
				</div>
				<div id="mobile-slider">
					<?php if ( have_rows( 'mobile_slider' ) ) : ?>
						<?php while ( have_rows( 'mobile_slider' ) ) : the_row(); ?>
							<div style="background-image: url(<?php echo get_sub_field('image')['sizes']['large']; ?>); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;" class="slide" ></div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<?php $dynamic_cats = get_terms( 'pwdynamic', array( 'hide_empty' => 0 ) ); 
					$middle = round( ( count($dynamic_cats) / 2 ) - 1 );
					foreach ( $dynamic_cats as $key => $value ) {
						if ( $value->slug === 'weddings' ){
							$old = array_splice($dynamic_cats, $key, 1);
							// Split old array in two and insert weddings into the middle, no matter where it was before.
							$dynamic_cats = array_merge( array_slice( $dynamic_cats, 0, $middle ), $old, array_slice( $dynamic_cats, $middle, null ) );
							break;
						}
					}
				?>

				<div id="dynamic-cats" class="dynamic-cats-wrap">
					<?php foreach ( $dynamic_cats as $cat ) : ?>
						<?php $term_image = wp_get_attachment_image_src( get_term_meta( $cat->term_id, 'image', true ), 'full' ); ?>
						<a href="<?php echo get_term_link( $cat ); ?>" class="dynamic-cat" style="background-image: url(<?php echo $term_image[0]; ?>);">
							<div class="inner">
								<div class="hover-wrapper">
									<div class="bottom-right"></div>
									<div><?php echo $cat->name; ?></div>
								</div>
							</div>
						</a>
					<?php endforeach; ?>
				</div>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="full-video-modal" class="mfp-hide magnific">
		<div class="inner">
			<?php the_field( 'video_modal_embed' ); ?>
		</div>
	</div>
	
	<script type="text/javascript">

		(function($){
			var $videoContainer = jQuery('#video-container'),
			 	$contentContainer = jQuery('#content'),
			 	$dynamicCatsContainer = jQuery('#dynamic-cats'),
			 	$window = jQuery(window),
			 	resizeEnd;

			 console.log('begin homepage resizing');

			$(window).on('resize', function() {

				clearTimeout(resizeEnd);

				resizeEnd = setTimeout(function() { 
						resizeVideoHeight();
				}, 100);

			});

			function resizeVideoHeight(){
				// console.log('resizeVideoHeight fired');
				// console.log( $window.width() );
				if ( $window.width() < 1250 ) {
					$videoContainer.height('');
					return;
				}
				// remove height from flex container for calcs
				$videoContainer.height(0);

				// set new video container height
				$videoContainer.height($contentContainer.height() - $dynamicCatsContainer.height() );
				// console.log($videoContainer.height());
			}

			// document.ready fires too early in Safari and causes gaps.
			$( document ).ready( function(){
				resizeVideoHeight();
			} );

			// Resize one more time for Safari
			$(window).on( 'load', function(){
				resizeVideoHeight();
			} );

			var bounceMenu = setInterval( function(){
				if ( $window.width() > 700 ){
					$('.menu-toggle .inner-text').toggleClass('bounce');
					$('.menu-toggle .fa').toggleClass('bounce');
				}
			}, 2000 );

			$(window).load( function(){
				var $blocks = $('.dynamic-cat');

				window.setTimeout( function(){
					initLightSweep( $blocks );
				}, 1000 );

				window.setInterval( function(){
					initLightSweep( $blocks );
				}, 8000 );

			} );

			function initLightSweep( $els ){
				for ( var i = 0; i < $els.length; i++ ){
					lightSweep( $els[i], i );
				}
			}

			function lightSweep( el, i ){
				var $el = $(el);
				window.setTimeout( function(){
					$el.addClass('light-sweep');
					window.setTimeout( function(){
						$el.removeClass('light-sweep');
					}, 500 );
				}, (100 * i) );
			}

			jQuery( window ).load( function(){
				jQuery('#menu-item-616 .dropdown-toggle').click();
			});
			
		})(jQuery);
	
	</script>
<?php get_footer();