<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Prairiewood
 */

if ( ! function_exists( 'prairiewood_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function prairiewood_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'prairiewood' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'prairiewood' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'prairiewood_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function prairiewood_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ' / ', 'prairiewood' ) );
		if ( $categories_list && prairiewood_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( '%1$s', 'prairiewood' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ' / ', 'prairiewood' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( '%1$s', 'prairiewood' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'prairiewood' ), esc_html__( '1 Comment', 'prairiewood' ), esc_html__( '% Comments', 'prairiewood' ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'prairiewood' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function prairiewood_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'prairiewood_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'prairiewood_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so prairiewood_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so prairiewood_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in prairiewood_categorized_blog.
 */
function prairiewood_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'prairiewood_categories' );
}
add_action( 'edit_category', 'prairiewood_category_transient_flusher' );
add_action( 'save_post',     'prairiewood_category_transient_flusher' );


function prairiewood_limit_words($words, $limit, $append = ' &hellip;') {
       // Add 1 to the specified limit becuase arrays start at 0
       $limit = $limit+1;
       // Store each individual word as an array element
       // Up to the limit
       $words = explode(' ', $words, $limit);
       // Shorten the array by 1 because that final element will be the sum of all the words after the limit
       array_pop($words);
       // Implode the array for output, and append an ellipse
       $words = implode(' ', $words) . $append;
       // Return the result
       return $words;
}

if ( ! function_exists( 'get_excerpt_by_id' ) ) :

/**
* Get Excerpt by ID
*
* Sometimes the functions get_excerpt() and the_excerpt() are not helpful, because they both only work in the Loop
* and return different markup (get_excerpt() strips HTML, while the_excerpt() returns content
* wrapped in p tags). This function will let you generate an excerpt by passing a post object:
*
* If there is a manually entered post_excerpt, it will return the content of the post_excerpt raw. Any markup
* entered into the Excerpt meta box will be returned as well, and you can use apply_filters('the_content', $output);
* on the output to render the content as you would the_excerpt().
*
* If there is no manual excerpt, the function will get the post_content, apply the_content filter, 
* escape and filter out all HTML, then truncate the excerpt to a specified length. 
*
* @since 1.0
* @param object|int $post
* @param int $length
*
*/
function get_excerpt_by_id( $post, $length = 55 ) {
	if ($post->post_excerpt) {
		return $post->post_excerpt;
	} elseif ( is_int( $post ) ) {
		$object = get_post( $post );
		if ( is_null($object) ){
			return '';
		}
	} else if ( is_object( $post ) ){
		$object = $post;
	} else {
		return '';
	}
	$output = $object->post_content;
	$output = apply_filters('the_content', $output);
	$output = str_replace('\]\]\>', ']]&gt;', $output);
	$output = strip_tags($output);
	$excerpt_length = 55;
	$words = explode(' ', $output, $length + 1);
	if (count($words)> $length) {
		array_pop($words);
		array_push($words, '');
		$output = implode(' ', $words);
	}
	return $output . '&hellip;';
}

endif;

