<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'pwproperty' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<style>
	    .galleria{ width: 700px; height: 400px; background: #000 }
	</style>
	<!-- <div class="galleria"></div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/galleria.js"></script> -->
	<script type="text/javascript">

		// jQuery(function($){
		// 	var images = $('.soliloquy-slides .soliloquy-item.soliloquy-image-slide .soliloquy-image').not( '.soliloquy-thumbnails-image');

		// 	var data = [];

		// 	window.setTimeout( function(){

		// 		for ( i  = 0; i < images.length ; i++ ){
		// 			console.log( images[i] );
		// 			data.push ( {
		// 				image: images[i].currentSrc
		// 			} );
		// 		}

		// 		console.log(data);

		// 		Galleria.loadTheme('/galleria/themes/classic/galleria.classic.min.js');

		// 		Galleria.run('.galleria', {
		// 		    dataSource: data,
		// 		    fullscreenCrop: true,
		// 		});
		// 		// $('.galleria').data('galleria').enterFullscreen();

		// 	}, 2000 );
			
		// });

	</script>

<?php
get_footer();
