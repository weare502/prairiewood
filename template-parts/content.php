<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

?>
<!-- Start content.php -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $bg_image = is_single() ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ) : array(''); ?>
	<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
		<div class="title-wrap">
			<?php
				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php prairiewood_posted_on(); ?>
				<?php prairiewood_entry_footer(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav"><span class="fa fa-angle-right"></span></span>', 'prairiewood' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'prairiewood' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php // prairiewood_entry_footer(); ?>
		<div class="seperator"></div>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<!-- End content.php -->