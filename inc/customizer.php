<?php
/**
 * Prairiewood Theme Customizer.
 *
 * @package Prairiewood
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function prairiewood_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_control( 'blogdescription' );

	$wp_customize->add_section( 'pw_contact', array(
		'priority' => 1,
		'title' => 'Prairiewood Options',
		'description' => 'Change your stuff information here.',
		'capability' => 'edit_pages'
	) );

	$wp_customize->add_setting( 'pw_phone_number', array(
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'sanitize_callback' => 'esc_html',
		'transport' => 'postMessage'
	) );

	$wp_customize->add_setting( 'pw_address', array(
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'sanitize_callback' => 'esc_html',
		'transport' => 'postMessage'
	) );

	$wp_customize->add_control( 'pw_phone_number', array(
	  'label' => __( 'Office Phone Number' ),
	  'type' => 'text',
	  'section' => 'pw_contact',
	) );

	$wp_customize->add_control( 'pw_address', array(
	  'label' => __( 'Office Address' ),
	  'type' => 'text',
	  'section' => 'pw_contact',
	) );

	$wp_customize->add_setting( 'pw_instagram_url', array(
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'sanitize_callback' => 'esc_html',
		'transport' => 'refresh'
	) );

	$wp_customize->add_control( 'pw_instagram_url', array(
	  'label' => __( 'Instagram URL' ),
	  'type' => 'text',
	  'section' => 'pw_contact',
	) );

	$wp_customize->add_setting( 'pw_facebook_url', array(
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'sanitize_callback' => 'esc_url',
		'transport' => 'refresh'
	) );

	$wp_customize->add_control( 'pw_facebook_url', array(
	  'label' => __( 'Facebook URL' ),
	  // 'description' => 'Include the @ sign!',
	  'type' => 'url',
	  'section' => 'pw_contact',
	) );

	$wp_customize->add_setting( 'pw_pinterest_url', array(
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'sanitize_callback' => 'esc_url',
		'transport' => 'refresh'
	) );

	$wp_customize->add_control( 'pw_pinterest_url', array(
	  'label' => __( 'Pinterest URL' ),
	  // 'description' => 'Include the @ sign!',
	  'type' => 'url',
	  'section' => 'pw_contact',
	) );

	// $wp_customize->add_setting( 'pw_full_map', array( 
	// 	'default' => '',
	// 	'type' => 'option',
	// 	'capability' => 'edit_pages',
	// 	'transport' => 'refresh'
	// ) );

	// $wp_customize->add_control( new WP_Customize_Image_Control (
 //       $wp_customize,
 //       'pw_full_map',
 //       array(
 //           'label'      => __( 'Full Preserve Map', 'theme_name' ),
 //           'section'    => 'pw_contact',
 //       )
 //   ) );

	$wp_customize->add_setting( 'pw_testimonials_page', array( 
		'default' => '',
		'type' => 'option',
		'capability' => 'edit_pages',
		'transport' => 'refresh'
	) );

	$wp_customize->add_control( 'pw_testimonials_page', array(
		'type' => 'select',
		'label' => 'Testimonails Page',
		'description' => 'Choose the page with the Testimonails Page Template',
		'section' => 'pw_contact',
		'choices' => pw_get_pages()
	) );

	// $wp_customize->add_setting( 'pw_contact_page', array( 
	// 	'default' => '',
	// 	'type' => 'option',
	// 	'capability' => 'edit_pages',
	// 	'transport' => 'refresh'
	// ) );

	// $wp_customize->add_control( 'pw_contact_page', array(
	// 	'type' => 'select',
	// 	'label' => 'Contact Page',
	// 	'description' => 'Choose the page with your contact info. This will be used to populate the \'Book Now\' button on a property page.',
	// 	'section' => 'pw_contact',
	// 	'choices' => pw_get_pages()
	// ) );
	

	// _details_button_text
	
	$pw_post_types = array( 'pwproperty', 'pwpackage', 'pwdynamicblock' );

	foreach ( $pw_post_types as $pt ){

		$obj = get_post_type_object( $pt );
		
		$wp_customize->add_setting( $pt . '_details_button_text', array(
			'default' => 'View More Details',
			'type' => 'option',
			'capability' => 'edit_pages',
			'sanitize_callback' => 'esc_html',
			'transport' => 'refresh'
		) );

		$wp_customize->add_control( $pt . '_details_button_text', array(
		  'label' => __( $obj->labels->singular_name . ' Details Button Text' ),
		  'type' => 'text',
		  'section' => 'pw_contact',
		) );

	}

}
add_action( 'customize_register', 'prairiewood_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function prairiewood_customize_preview_js() {
	wp_enqueue_script( 'prairiewood_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'prairiewood_customize_preview_js' );
