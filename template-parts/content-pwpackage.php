<?php
/**
 * Template part for displaying packages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Prairiewood
 */

?>
<!-- Start contnet-page.php -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<header class="entry-header" style="background-image: url(<?php echo $bg_image[0]; ?>);" data-bg-image="<?php echo $bg_image[0]; ?>">
		<div class="title-wrap">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header><!-- .entry-header -->

	<!-- <div class="package-meta">
		<div class="content-wrapper">	

			<div class="social">
				<a href="tel:<?php echo get_option( 'pw_phone_number' ); ?>" class="icon print"><span class="fa fa-phone"><span class="screen-reader-text">Call Us</span></span></a>
				<a href="#resources-popup" class="icon print open-popup-link"><span class="fa fa-envelope-o"><span class="screen-reader-text">Get Resources</span></span></a>
				<a href="javascript:window.print();" class="icon print"><span class="fa fa-print"><span class="screen-reader-text">Print this page</span></span></a>
				<a href="<?php the_permalink(); ?>" class="icon link"><span class="fa fa-link"><span class="screen-reader-text">Link to this page</span></span></a>
				<a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&description=<?php echo urlencode(get_the_title());?>&media=<?php echo $bg_image[0]; ?>" target="_blank" class="icon pinterest"><span class="fa fa-pinterest"><span class="screen-reader-text">Pin this page on Pinterest</span></span></a>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" class="icon facebook"><span class="fa fa-facebook-square"><span class="screen-reader-text">Share this page on Facebook</span></span></a>
			</div>

		</div>
	
	</div> -->

	<div class="entry-content content-wrapper">

		<div id="package-main-content">

				<?php the_content(); ?>

				<?php if ( get_post_meta( get_the_ID(), 'pwpackage_resource_form', true ) ) : ?>
					<p class="centered-text">
						<?php echo get_post_meta( get_the_ID(), 'pwpackage_resource_intro', true ); ?>
						<br/>
						<br/>
						<a href='#resources-popup' class="open-popup-link button">Request Information</a>
					</p>
				<?php endif; ?>

		</div><!-- #package-main-content -->

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'prairiewood' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->

	<div id="resources-popup" class="mfp-hide magnific">
		<div class="inner">
			<?php 

				$form_id = get_post_meta( get_the_ID(), 'pwpackage_resource_form', true );

				if ( function_exists('gravity_form' ) ) :
					gravity_form( $form_id, false, false, false, null, true );
				endif;

			?>
		</div>
	</div>

</article><!-- #post-## -->
<!-- End content-page.php -->